version := "1.0.0"
name := "swagger-scala-akka-http-server"
organization := "io.swagger"
scalaVersion := "2.12.12"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http"            % "10.1.10",
  "com.typesafe.akka" %% "akka-stream"          % "2.5.26",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.10",
  "com.oracle.ojdbc"  %  "ojdbc8"               % "19.3.0.0",
)
