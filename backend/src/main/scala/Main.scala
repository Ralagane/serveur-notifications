import model._
import model.Email
import serveur.api._
import acteur.ServeurDesMessages
import server._
import akka.actor.{ActorLogging, ActorSystem}
import akka.pattern.ask
import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import spray.json.{DefaultJsonProtocol, JsObject}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import scala.concurrent.duration._
import scala.io.StdIn
import akka.http.scaladsl.server.Directives._

/**
 * La classe main instancialise les acteurs et le routes et définie chaque fonction définie dans la classe GestionRequeteApi.
 */
object Main extends App 
{
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val defaultTimeout = Timeout(600000 seconds)
  // se connecter à la bdd oracle
  //val laBDD = new BaseDeDonnees()
  val leServeur = new ServeurDesMessages(system)
  val api = new GestionDesRoutes(GestionRequeteApi, DefaultMarshaller)
  
  val host = "localhost"
  val port = 8080

  val bindingFuture = Http().bindAndHandle(pathPrefix("api"){api.route}, host, port)
  println(s"Server disponible à l'addresse suivante :  http://${host}:${port}/\nPour arreter le serveur appuyer sur entrer...")

  bindingFuture.failed.foreach 
  { 
    ex => println(s"${ex} impossible de se connecter à l'addresse  ${host}:${port}!")
  }

  StdIn.readLine()
  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())


  object GestionRequeteApi extends NotificationApiService
  {
    import acteur.AgentDesMessages
    
    def postAuthentification(emailetMdp: String): Route =
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.authentification(emailetMdp)).mapTo[String]
      requestcontext =>
      {
        reponse.flatMap
        {
          (uneReponse : String) =>
          {
            uneReponse match
            {
              case "erreur" => complete("")(requestcontext)
              case token =>  complete(token)(requestcontext)
            }
          }
        }
      }
    }
    /**
      * La requête pour creer un client en base.
      *
      * @param body les informations du client à creer
      * @return la réponse du serveur vis à vis de ça.
      */
    def postCompteclient(body: Client): Route =
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.creerCompteClient(body)).mapTo[String]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (unToken: String) => 
          {
            unToken match 
            {
              case "roblox" => postCompteclient403(requestcontext) // le cas erreur 
              case  unecle =>  postCompteclient201(unecle)(requestcontext) // une cle
            }
          }
        }
      }
    }
    /**
      * La requête pour supprimer un client en base.
      *
      * @param body le token du client pour le supprimer
      * @return la réponse du serveur vis à vis de ça.
      */
    def deleteCompteClient(body : String): Route =
    {
      val reponse = (leServeur.agentDesMessages? AgentDesMessages.supprimerCompteDuClient(body)).mapTo[Boolean]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (resultat : Boolean) =>
          {
            resultat match
            {
              case true =>  deleteCompteClient200 (requestcontext)
              case false => deleteCompteClient404 (requestcontext)
            }
          }
        }
      }
    }
    /**
      * La requête pour créer une API en base.
      *
      * @param body les informations de l'API
      * @return la réponse du serveur vis à vis de ça.
      */
    def postCompteapi(body: ApiTierce): Route=
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.creerCompteApi(body)).mapTo[String]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (unToken: String) => 
          {
            unToken match 
            {
              case "philipe" => postCompteapi403(requestcontext)
              case uneCleApi => postCompteapi201(uneCleApi)(requestcontext)
              
            }
          }
        }
      }
    }
    /**
      * La requête pour supprimer une API en base.
      *
      * @param tokenClient le token de l'api a supprimer
      * @return la réponse du serveur vis à vis de ça.
      */
    def deleteCompteApi(tokenClient: String): Route =
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.supprimerCompteDeAPI(tokenClient)).mapTo[Boolean]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (reponseDuServeur : Boolean) =>
          {
            reponseDuServeur match 
            {
              case false =>  deleteCompteApi404(requestcontext)
              case true  =>  deleteCompteApi200(requestcontext)
            }
          }
        }
      }
    }
    def postEnvoyernotification(body: Notification) ( implicit  toEntityMarshallerEmailListe : ToEntityMarshaller[List[Email]]): Route = 
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.envoyerUneNotification(body)).mapTo[Tuple2[Int,List[Email]]]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (reponseDuServeur : Tuple2[Int,List[Email]]) =>
          {
            reponseDuServeur._1 match 
            {
              case 200 => postEnvoyernotification200 (requestcontext)
              case 401 => postEnvoyernotification401 (requestcontext)
              case 404 => postEnvoyernotification404 (reponseDuServeur._2)(toEntityMarshallerEmailListe)(requestcontext)
            }
          }
        }
      }   
    }
    def getNotificationsnonLues(tokenClient: String)(implicit toEntityMarshallerNotificationClientListe: ToEntityMarshaller[List[NotificationClient]]): Route =
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.notificationNonLues(tokenClient)).mapTo[Tuple2[Int,List[NotificationClient]]]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (reponseDuServeur : Tuple2[Int,List[NotificationClient]]) =>
          {
            reponseDuServeur._1 match 
            {
              case 200 => 
              { 
                complete(reponseDuServeur._2)(requestcontext)
              }
              case 401 => getNotificationsnonLues401 (requestcontext)
              case 404 => complete(reponseDuServeur._2)(requestcontext)
            }
          }
        }
      }
    }
    def postMettreajourunetatdermise(body : EtatDeRemiseClient): Route =
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.metteAJourEtatDeRemise(body)).mapTo[Int]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (reponseDuServeur : Int) =>
          {
            reponseDuServeur match 
            { 
              case 200 => postMettreajourunetatdermise200  (requestcontext)
              case 401 => postMettreajourunetatdermise401  (requestcontext)
              case 403 => postMettreajourunetatdermise403  (requestcontext)
              case 402 => complete(402,"avant de mettre a jour une notification, il faut la lire...")(requestcontext)
              case 404 => postMettreajourunetatdermise404  (requestcontext)
            }
          }
        }
      } 
    }
    def getEtatderemise(tokenApplicationTiers: String, identifiantNotification: String)(implicit toEntityMarshallerEtatDeRemiseApiListe: ToEntityMarshaller[List[EtatDeRemiseApi]]): Route =
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.consulterEtatDeRemise(tokenApplicationTiers, identifiantNotification)).mapTo[Tuple2[Int,List[EtatDeRemiseApi]]]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (reponseDuServeur : Tuple2[Int, List[EtatDeRemiseApi]]) =>
          {
            reponseDuServeur._1 match 
            {
              case 200 => getEtatderemise200(reponseDuServeur._2) (toEntityMarshallerEtatDeRemiseApiListe) (requestcontext)
              case 401 => getEtatderemise401 (requestcontext)
              case 403 => getEtatderemise403 (requestcontext)
              case 404 => getEtatderemise404 (requestcontext)
            }
          }
        }
      }
    }
    def getStatistique(tokenApplicationTiers: String) (implicit toEntityMarshallerStatistiqueList : ToEntityMarshaller[List[Statistique]]): Route =
    {
      val reponse = (leServeur.agentDesMessages ? AgentDesMessages.voirStatistique(tokenApplicationTiers)).mapTo[Tuple2[Int,List[Statistique]]]
      requestcontext =>
      {
        (reponse).flatMap
        {
          (reponseDuServeur : Tuple2[Int, List[Statistique]]) =>
          {
            reponseDuServeur._1 match
            {
              case 200 => getStatistique200(reponseDuServeur._2)(toEntityMarshallerStatistiqueList)(requestcontext)
              case 401 => getStatistique401(requestcontext)
              case 404 => getStatistique404(requestcontext)
            }
          }
        }
      }
    }
  }

}

object DefaultMarshaller extends NotificationApiMarshaller with SprayJsonSupport
{
  import DefaultJsonProtocol._
  import spray.json._

  // ce que le front-end envoie
  def clientFormat: RootJsonFormat[Client] = jsonFormat6(Client)
  def apiTierceFormat: RootJsonFormat[ApiTierce] = jsonFormat3(ApiTierce)
  def notificationFormat: RootJsonFormat[Notification] = jsonFormat4(Notification)
  def notificationClientFormat : RootJsonFormat[NotificationClient] = jsonFormat4(NotificationClient)
  def etatDeRemiseClientFormat : RootJsonFormat[EtatDeRemiseClient] = jsonFormat3(EtatDeRemiseClient)

  // ce que le back end envoie
  implicit def toEntityMarshallerEmailListe : ToEntityMarshaller[List[Email]] = listFormat(jsonFormat1(Email))
  implicit def toEntityMarshallerEtatDeRemiseApiListe: ToEntityMarshaller[List[EtatDeRemiseApi]] = listFormat(jsonFormat2(EtatDeRemiseApi))
  implicit def toEntityMarshallerStatistiqueList : ToEntityMarshaller[List[Statistique]]= listFormat(jsonFormat6(Statistique))
  implicit def toEntityMarshallerNotificationClientListe :ToEntityMarshaller[List[NotificationClient]] = listFormat(jsonFormat4(NotificationClient))   
}
