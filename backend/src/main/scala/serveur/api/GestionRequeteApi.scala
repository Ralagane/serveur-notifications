package serveur.api

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import spray.json.{DefaultJsonProtocol, JsObject, RootJsonFormat}
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport

import model.ApiTierce
import model.Client
import model.Notification
import model.Statistique
import model.Email
import model.NotificationClient
import model.EtatDeRemiseApi
import model.EtatDeRemiseClient

/**
 * GestionDesRoutes est la classe fait le lien entre l'api et les acteurs. Elle spécifie les routes et pour chacune la fonction des acteurs
 * à appeler.
 * Cette classe possède  l'interface (trait) NotificationApiService pour définir les fonctions des routes sans les implémenter, la classe main s'en chargera.
 * Cette classe possède aussi le trait NotificationApiMarshaller pour définir les donnnées envoyées et reçues entre l'api et le serveur.
 */
class GestionDesRoutes(notificationApiService: NotificationApiService, defaultMarshaller: NotificationApiMarshaller) extends SprayJsonSupport
{
  import defaultMarshaller._

  lazy val route: Route =
    path("authentification"/ Segment/ )
    {
      (emailEtMotDePasse) => // être moins spécifique sur ce qu'est un token ?
      get
      {
        notificationApiService.postAuthentification(emailEtMotDePasse = emailEtMotDePasse)
      }
    }~
    path("creercompteapi") {
      post 
      {
        entity(as[ApiTierce])
        { 
          body => notificationApiService.postCompteapi(body = body)
        }
      }
    } ~
    path("creercompteclient") 
    {
      post 
      {

        entity(as[Client])
        { 
          body => notificationApiService.postCompteclient(body = body)
        }
             
      }
    } ~
    path("envoyernotification") 
    {
      post 
      {
        entity(as[Notification])
        { 
          body => notificationApiService.postEnvoyernotification(body = body)
        }
      }
    } ~
    path("etatderemise" / Segment / Segment) 
    { 
      (tokenApplicationTiers, identifiantNotification) => 
        get 
        {
          notificationApiService.getEtatderemise(tokenApplicationTiers = tokenApplicationTiers, identifiantNotification = identifiantNotification)
        }
    } ~
    path("mettreajourunetatdermise") 
    { 
      post 
      {
        entity(as[EtatDeRemiseClient])
        {
          body => notificationApiService.postMettreajourunetatdermise(body)
        }  
      }
    } ~
    path("notificationsnonLues" / Segment) 
    { 
      (tokenClient) => 
        get 
        {
          notificationApiService.getNotificationsnonLues(tokenClient = tokenClient)
        }  
    } ~
    path("statistique" / Segment) 
    { 
      (tokenApplicationTiers) => 
        get 
        {
          notificationApiService.getStatistique(tokenApplicationTiers = tokenApplicationTiers)
        }
    } ~
    path("supprimercompteapi" / Segment) 
    { 
      (tokenApiTierce) => 
        delete 
        { 
          notificationApiService.deleteCompteApi(tokenApiTierce = tokenApiTierce)
        }
    } ~
    path("supprimercompteclient" / Segment) 
    { 
      (tokenClient) => 
        delete 
        {
          notificationApiService.deleteCompteClient(tokenClient = tokenClient)     
        }
    }
}

trait NotificationApiService 
{
  /**
    * la requête pour s'authentifier
  **/
  def postAuthentification(emailEtMotDePasse : String): Route

  /**
   * La requête creer un client et les réponses du serveur.
   * Code: 201, Message: le client a bien été créé. L'API renvoie son token, DataType: String
   * Code: 400, Message: le client a mal remplie les informations. La requête préciera quoi., DataType: String (traiter dans une prochaine évolution)
   * Code: 403, Message: Le courrier électronique est déjà utilisé.
   */
  def postCompteclient(body: Client): Route
  def postCompteclient201(responseString: String): Route = complete((201, responseString))
  //def creercompteclientPost400(responseString: String): Route = complete((400, responseString))
  def postCompteclient403: Route = complete((403, "Le courrier électronique est déjà utilisé."))

  /**
   * La requête pour supprimer le client en base et les réponses du serveur.
   * Code: 200, Message: le compte a bien été supprimé.
   * Code: 404, Message: Aucun compte trouvé avec ce token.
   */
  def deleteCompteClient(tokenClient: String): Route
  def deleteCompteClient200: Route = complete((200, "le compte a bien été supprimé."))
  def deleteCompteClient404: Route = complete((404, "Aucun compte trouvé avec ce token client."))

  /**
   * La requête pour creer une api en base et les réponses du serveur.
   * Code: 201, Message: le compte de l'api tierce a bien été créé., DataType: String
   * Code: 400, Message: l'api a mal remplie les informations. La requête préciera quoi., DataType: String (traiter dans une prochaine évolution)
   * Code: 403, Message: l'organisation est déjà inscrite en base >:(
   */
  def postCompteapi(body: ApiTierce): Route
  def postCompteapi201(responseString: String): Route = complete(responseString)
  //def creercompteapiPost400(responseString: String): Route = complete((400, responseString))
  def postCompteapi403: Route = complete((403, "l'organisation est déjà inscrite en base >:("))

  /**
   * La requête pour supprimer une api en base et les réponses du serveur.
   * Code: 200, Message: le compte a bien été supprimé
   * Code: 404, Message: aucun compte trouvé avec ce token
   */
  def deleteCompteApi(tokenApiTierce: String): Route
  def deleteCompteApi200: Route = complete((200, "le compte a bien été supprimé"))
  def deleteCompteApi404: Route = complete((404, "Aucun compte trouvé avec ce token api."))

  /**
   * La requête envoyer une notification et les réponses de l'api.
   * Code: 200, Message: Notification créée.
   * Code: 401, Message: Echec d'authentification. L'appplication n'a pas été reconnue.
   * Code: 404, Message: La notification n'a pas pu être envoyé à certains utilisateurs (retourne les emails introuvables), DataType: List[String]
   */
  def postEnvoyernotification(body: Notification) ( implicit  toEntityMarshallerEmailListe : ToEntityMarshaller[List[Email]]): Route
  def postEnvoyernotification200: Route = complete((200, "Notification créée."))
  def postEnvoyernotification401: Route = complete((401, "Echec d'authentification. L'appplication n'a pas été reconnue."))
  def postEnvoyernotification404(responseListeEmails: List[Email])(implicit toEntityMarshallerEmailListe: ToEntityMarshaller[List[Email]]): Route = complete(responseListeEmails)

   /**  
   * La requête pour consulter les notifications non lues et les réponses de l'api.
   * Code: 200, Message: Les notifications ont été récupérées, DataType: List[Notification]
   * Code: 400, Message: Aucune notification récupérée.
   * Code: 401, Message: Echec de l''authentification, le client n'a pas été reconnu.
   */
  def getNotificationsnonLues(tokenClient: String) (implicit toEntityMarshallerNotificationClientListe: ToEntityMarshaller[List[NotificationClient]]): Route
  def getNotificationsnonLues200(responseNotificationarray: List[NotificationClient])(implicit toEntityMarshallerNotificationClientListe: ToEntityMarshaller[List[NotificationClient]]): Route = complete(responseNotificationarray)
  def getNotificationsnonLues404: Route = complete((404, "Aucune notification récupérée.")) // renvoyer les listes
  def getNotificationsnonLues401: Route = complete((401, "Echec de l''authentification, le client n'a pas été reconnu."))

   /**
   * La requête pour mettre à jour un état de remise et les réponses de l'API.
   * Code: 200, Message: La notification a vue son état de remise mis à jour.
   * Code: 401, Message: Echec d'authentification. Le client n'a pas été reconnu.
   * Code: 404, Message: La notification est introuvable.
   * code: 403, Message: a notification a été trouvée mais n'appartient pas au client >:(
   */
  def postMettreajourunetatdermise(etatDeRemiseClient : EtatDeRemiseClient): Route
  def postMettreajourunetatdermise200: Route = complete((200, "La notification a vue son état de remise mis à jour."))
  def postMettreajourunetatdermise401: Route = complete((401, "Echec d'authentification. Le client n'a pas été reconnu."))
  def postMettreajourunetatdermise404: Route = complete((404, "La notification est introuvable."))
  def postMettreajourunetatdermise403: Route = complete((403, "La notification a été trouvée mais n'appartient pas au client >:(."))
  
  
  /**
   * La requête pour que l'api reçoive un état de remise d'une notification et les réponses de l'api
   * Code: 200, Message: liste envoyé, DataType: List[EtatDeRemise]
   * Code: 401, Message: Echec d'authentification. L'appplication n'a pas été reconnue.
   * Code: 404, Message: Notification introuvable.
   */
  def getEtatderemise(tokenApplicationTiers: String, identifiantNotification: String) (implicit toEntityMarshallerEtatDeRemiseApiListe: ToEntityMarshaller[List[EtatDeRemiseApi]]): Route
  // la 200 est à tester (todo)
  def getEtatderemise200(responseEtatDeRemisearray: List[EtatDeRemiseApi])(implicit toEntityMarshallerEtatDeRemiseApiListe: ToEntityMarshaller[List[EtatDeRemiseApi]]): Route = complete(responseEtatDeRemisearray)
  def getEtatderemise401: Route = complete((401, "Echec d'authentification. L'appplication n'a pas été reconnue."))
  def getEtatderemise403: Route = complete((403, "La notification a été trouvée mais n'appartient pas au client >:(."))
  def getEtatderemise404: Route = complete((404, "Notification introuvable."))


  /**
   * Code: 200, Message: Statistique existante envoyée, DataType: List[Statistique]
   * Code: 401, Message: Echec de l'authentification. L'application n'a pas été reconnue.
   * Code: 404, Message: Aucune statistique existante
   */
  def getStatistique(tokenApplicationTiers: String) (implicit toEntityMarshallerStatistiqueList : ToEntityMarshaller[List[Statistique]]): Route
  def getStatistique200(responseStatistiquearray: List[Statistique])(implicit toEntityMarshallerStatistiqueList : ToEntityMarshaller[List[Statistique]]): Route = complete(responseStatistiquearray)
  def getStatistique401: Route = complete((401, "Echec de l'authentification. L'application n'a pas été reconnue.")) // voir comment gérer les erreurs
  def getStatistique404: Route = complete((404, "Aucune statistique existante"))
}

trait NotificationApiMarshaller 
{
  // ce que le front-end envoie
  implicit def clientFormat: RootJsonFormat[Client]
  implicit def apiTierceFormat: RootJsonFormat[ApiTierce]
  implicit def notificationFormat: RootJsonFormat[Notification]
  implicit def etatDeRemiseClientFormat: RootJsonFormat[EtatDeRemiseClient]

  // ce que le back end envoie
  implicit def toEntityMarshallerStatistiqueList : ToEntityMarshaller[List[Statistique]]
  implicit def toEntityMarshallerEmailListe : ToEntityMarshaller[List[Email]]
  implicit def toEntityMarshallerNotificationClientListe: ToEntityMarshaller[List[NotificationClient]]
  implicit def toEntityMarshallerEtatDeRemiseApiListe: ToEntityMarshaller[List[EtatDeRemiseApi]]
  
}




