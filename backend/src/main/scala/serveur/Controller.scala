package server

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import serveur.api.GestionDesRoutes
import akka.http.scaladsl.server.Directives._
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

class GestionRequeteApi (default: GestionDesRoutes)(implicit system: ActorSystem, materializer: ActorMaterializer) 
{
    lazy val routes: Route = default.route 
    Http().bindAndHandle(routes, "0.0.0.0", 9000)
}