package model
/**
 * Cette classe permet de retourner un email
 * ne se trouvant pas en base ne données.
 *
 * @param email le fameux email non trouvable en base.
 */
case class Email 
(
  email: String,
)
/**
  * Des fonctions utiles manipulants l'objet email.
  */
object LeEmail
{
  /**
    * Petite fonction convertissant une liste String en une liste Email pour l'envoyer à l'API.
    *
    * @param uneListe la liste à convertir
    * @return une liste d'Emails
    */
  def toEmail(uneListe :  List[String]): List[Email]  = toEmail2(uneListe,List.empty[Email])
  def toEmail2(uneListe : List[String],accumulateur :List[Email]) : List[Email] =
  {
    uneListe match 
    {
      case tete:: Nil => List(Email(tete))
      case tete:: reste => toEmail2(reste, accumulateur.::(Email(tete)))
      case Nil => List.empty
    }
  }
}