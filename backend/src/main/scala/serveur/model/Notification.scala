package model


/**
 * Une notification est un message à l'intention d'un ou plusieurs client(s).
 *
 * @param emetteur l'email de l'api tierce qui envoie cette notification
 * @param identifiantNotification l'identifiant unique de la notification
 * @param message le message à l'intention d'un ou plusieurs utilisateur(s)
 * @param listeDeCourriers une liste courriers d'utilisateurs devant reçevoir cette notification.
 */
case class Notification 
(
  emetteur: String,
  identifiantNotification: String,
  message: String,
  listeDeCourriers: List[String]
)

