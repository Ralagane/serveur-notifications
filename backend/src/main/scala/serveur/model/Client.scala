package model


/**
 * Un client avec ses informations.
 *
 * @param nom 
 * @param prenom 
 * @param age 
 * @param sexe 
 * @param email 
 * @param motDePasse 
 */
case class Client 
(
  nom: String,
  prenom: String,
  age: Int,
  sexe: String,
  email: String,
  motDePasse: String
)

