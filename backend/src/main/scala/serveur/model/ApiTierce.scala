package model


/**
 * Une api tierce avec ses informations.
 *
 * @param organisation 
 * @param motDePasse 
 * @param email 
 */
case class ApiTierce (
  organisation: String,
  motDePasse: String,
  email: String
)

