package model


/**
 * l'état de remise d'une application, est-elle réçue ? Vue par le client ?
 *
 * @param idNotification l'identifiant de la notification
 * @param message le message de la notification
 * @param nombreUtilisateurs le nombre d'utilisateurs total qui sont censés reçevoir la notification
 * @param lu  le nombre d'utilisateurs ayant lus le message
 * @param pasLu le nombre d'utilisateurs ayant pas lus le message
 * @param recu le nombre d'utilisateurs ayant reçus le message mais qui ne l'ont pas lu.
 */
case class Statistique 
(
  idNotification: String,
  message: String,
  nombreUtilisateurs: Int,
  lu: Int,
  pasLu: Int,
  recu: Int
)
