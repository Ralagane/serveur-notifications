package model

case class EtatDeRemise
(
  identifiantClient : String,
  etatDeRemise: String
)