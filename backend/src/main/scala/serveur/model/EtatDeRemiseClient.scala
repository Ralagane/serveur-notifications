package model

case class EtatDeRemiseClient 
(
  tokenClient : String,
  identifiantNotification : String,
  nouveauEtatDeRemise : String
)
