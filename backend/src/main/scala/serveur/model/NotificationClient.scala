package model

final case class NotificationClient
(
  organisation : String,
  identifiantNotification : String,
  message : String,
  etatDeRemise : String
)