package utilitaire
import java.security.MessageDigest
/**
  * Cette classe rassemble toutes les méthodes fortes utile pour les autres classes mais qui 
  * leurs définitions allonge le code. Donc pour des raisons de visibilité elles seront stockées
  * dans la boite à outils.
  */

object BoiteAOutils
{
  /**
    * C'est la méthode eco+ pour crypter un email mais ce n'est pas du tout pratique quand on veut décrypter pour comparer les token par exemple
    * Cette méthode est prise sur le net permettant de générer une chaine de caractère aléatoirement
    * source : https://alvinalexander.com/scala/creating-random-strings-in-scala/ (un très bon site, j'envisage sans doute à acheter le livre)
    *
    * @param length la taille de la clé.
    * @return retourne une chaine de caractère aléatoirement définie.
    */
  def randomString(length: Int) : String = 
  {
    val r = new scala.util.Random
    val sb = new StringBuilder
    for (i <- 1 to length) 
    {
        sb.append(r.nextPrintableChar)
    }
    sb.toString
  }
  /**
    * Version améliorer de random stram prenant cette fois-ci les lettres de a-Z.
    *
    * @return une chaine de 8 caractères faite aléatoirement.
    */
  def randomStringAmeliore() : String = (1 to 8).map(_ => (scala.util.Random.alphanumeric.filter(_.isLetter).head)).mkString
  /**
    * cette petite fonction permet de crypter un email+mdp en SHA1
    *
    * @param identifiant l'email+mdp à crypter
    * @return l'identfiant crypté et donc le token
    */
  def genererTokenSHA1(identifiant : String): String = MessageDigest.getInstance("SHA-1").digest(identifiant.getBytes("UTF-8")).map("%02x".format(_)).mkString
 
 


}
