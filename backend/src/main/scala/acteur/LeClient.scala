package acteur

import akka.actor.{ Props, ActorSystem, Actor, ActorLogging, ActorRef, PoisonPill }
import model._
import model.Notification // obligé de préciser la notification car le compilateur confond avec un type Notification en javax
import java.util.Date

// class test !
object LeClient
{
  // "Lemoine","Jean",25,"Masculin","jean.lemoine@couscous.com" Todo : faire un regex qui test si l'email est valide
  def apply(unAgentDeMessage: ActorRef): Props = Props(new LeClient(unAgentDeMessage))
}
class LeClient(unAgentDeMessage : ActorRef) extends Actor with ActorLogging
{
  import LeClient._
  import AgentDesMessages._
  
  def receive: Receive = 
  {
    case msg @ _ => log.info(s"j'ai recu le message, askip le client est bon et son token est : $msg") 
  }

  override def preStart(): Unit = 
  {
    log.info(s"je vais creer un client :)")
    unAgentDeMessage ! creerCompteClient(Client("Lemoine","Jean",25,"Masculin","Jean.Lemoine@couscous.com","toto"))
  }
}
