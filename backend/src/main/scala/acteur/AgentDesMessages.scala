package acteur

import akka.actor.{ Props, ActorSystem, Actor, ActorLogging, ActorRef, PoisonPill }
import model._
import model.Notification // obligé de préciser la notification car le compilateur confond avec un type Notification en javax
import java.util.Date

/**
 * Un AgentDesMessages dialogue avec le client ou une API tierce, prend ses requêtes et délègue le sale boulot à un esclave.
 * connait : un esclave pour déléguer les tâches
 * reçoit : les demandes du client
 * au démarrage : ne fait rien, il attend d'être notificié.
 * Cette classe possède un objet compagnion.
 */

object AgentDesMessages
{ 
  // les actions du client
  case class creerCompteClient(unClient : Client)
  case class notificationNonLues(tokenClient : String)
  case class metteAJourEtatDeRemise(unEtatDeRemise: EtatDeRemiseClient)
  case class supprimerCompteDuClient(tokenClient: String)
  case object Bonjour // ce n'est pas une requête, c'est pour tester l'implémentation avec un client test
  case class authentification(emailEtMdp: String)

  // les actions d'une APi
  case class creerCompteApi(uneApiTierce : ApiTierce)
  case class envoyerUneNotification(uneNotification: Notification)
  case class voirStatistique(tokenApi : String)
  case class supprimerCompteDeAPI(tokenApi: String)
  case class consulterEtatDeRemise(tokenApi : String, identifiantNotification : String) /// faut le définir lui

  // Ce que l'agent reçoit quand l'esclave a fini de traiter les demandes du client
  case class reponseCreerCompteClient(client : ActorRef,reponse : String)
  case class reponseNotificationNonLues (uneListeDeNotification: List[NotificationClient],client: ActorRef,typeReponse : Int)
  case class reponsemetteAJourEtatDeRemise (client: ActorRef, typeReponse : Int)
  case class reponseSupprimerCompteClient(client : ActorRef, typeReponse: Boolean)
  case class reponseAuthentification(reponse : String, client : ActorRef)
  
  // Ce que l'agent reçoit quand l'esclave a fini de traiter les demandes de l'Api Tierce
  case class reponseCreerCompteApi(reponse : String,apitierce : ActorRef)
  case class reponseEnvoyerUneNotificationAPI(listeIdClientsNonReconnues : List[Email],apitierce: ActorRef, typeReponse : Int)
  case class reponseVoirStatistique(uneListeDeStat : List[Statistique], apitierce : ActorRef, typeReponse: Int)
  case class reponseSupprimerCompteApi(apitierce: ActorRef, typeReponse : Boolean)
  case class reponseConsulterEtatDeRemise(listeEtatDeRemiseApi : List[EtatDeRemiseApi], apitierce: ActorRef, typeReponse : Int )

  def apply(unEsclave : ActorRef): Props = Props(new AgentDesMessages(unEsclave))
}

class AgentDesMessages(unEsclave : ActorRef) extends Actor with ActorLogging 
{
  import BaseDeDonneesEcoPlus._
  import AgentDesMessages._
  import Esclave._

  def receive : Receive =
  {
    // les demandes du client
    case creerCompteClient(unClient) => unEsclave ! creerCompteDuClient(unClient, sender)
    case notificationNonLues(unTokenClient) => unEsclave ! notificationNonLuesClient(unTokenClient, sender)
    case metteAJourEtatDeRemise (unEtatDeRemiseClient) => unEsclave !  metteAJourEtatDeRemiseClient(unEtatDeRemiseClient, sender)
    case supprimerCompteDuClient(tokenClient) => unEsclave ! supprimerCompteClient(tokenClient, sender)
    case Bonjour => sender ! log.info(s"Bonjour à toi mon chère client : $sender")
    case authentification(emailEtMdp) => unEsclave ! sauthentifierClient(emailEtMdp, sender)

    // les demandes de l'api
    case creerCompteApi(uneApiTierce) =>  unEsclave ! creerCompteDeApi(uneApiTierce, sender)
    case envoyerUneNotification (uneNotification) =>unEsclave ! envoyerUneNotificationAPI(uneNotification, sender)
    case voirStatistique(tokenApi) => unEsclave ! voirStatistiqueAPI(tokenApi, sender)
    case supprimerCompteDeAPI(unTokenApi) => unEsclave ! supprimerCompteAPI(unTokenApi, sender)
    case consulterEtatDeRemise(tokenApi, identifiantNotification) => unEsclave ! consulterEtatDeRemiseApi(tokenApi,identifiantNotification,sender)
    
    // Ce que l'agent reçoit quand l'esclave a fini de traiter les demandes du client
    case reponseCreerCompteClient(client, reponse) => client ! reponse // code erreur : roblox
    case reponseSupprimerCompteClient(client,reponse)=> client ! reponse // renvoie bool
    case reponseNotificationNonLues (uneListeDeNotification,client,typeReponse) => client ! (typeReponse, uneListeDeNotification) // renvoie une map à changer
    case reponsemetteAJourEtatDeRemise (client, typeReponse ) =>client ! typeReponse // renvoie un int
    case reponseAuthentification(reponse, client) => client ! reponse

    // Ce que l'agent reçoit quand l'esclave a fini de traiter les demandes de l'Api Tierce
    case reponseCreerCompteApi(reponse,apitierce) => apitierce ! reponse
    case reponseSupprimerCompteApi(apitierce, reponse)=> apitierce ! reponse
    case reponseEnvoyerUneNotificationAPI(listeIdClientsNonReconnues,apitierce, typeReponse) => apitierce ! (typeReponse, listeIdClientsNonReconnues)
    case reponseVoirStatistique(reponse, apitierce, typeReponse) => apitierce ! (typeReponse, reponse) // renvoie une stat
    case reponseConsulterEtatDeRemise(reponse, apitierce, typeReponse) => apitierce ! (typeReponse, reponse)
  }
}