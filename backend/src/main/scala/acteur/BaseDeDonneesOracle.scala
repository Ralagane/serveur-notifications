package acteur

import oracle.jdbc.pool.OracleDataSource
import java.sql.Connection
import java.sql.DatabaseMetaData
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import akka.actor.{ Props, ActorSystem, Actor, ActorLogging, ActorRef, PoisonPill }

/**
  * BaseDeDonnesOracle permet de se connecter à une vrai base de données et faire des requêtes
  * en fonction de ce que demande un client, l'api ou le serveur en cas de traitement.
  * Elle n'est pas implémentée pour le moment mais c'est prévu lors de la mise a à jour 2.0 de l'api
  * (je dois déjà avoir une note donc faire un truc simple)
  */
object  BaseDeDonneesOracle 
{
  // Config de la BDD (une vraie bdd)
  val identifiant : String = "acteur_bdd"
  val motDePasse: String  = "acteur_bdd"
  val urlBdd : String  = "jdbc:oracle:thin:@localhost:1521/xe"

  def apply(): Props = Props(new BaseDeDonneesOracle())
}

class BaseDeDonneesOracle extends Actor with ActorLogging
{
  import BaseDeDonneesOracle._
   def receive: Receive = 
  {
    case msg @ _ => log.info(s"j'ai recu le message : $msg") 
  }

  override def preStart(): Unit = 
  {
    connexion()
  }

  def connexion() : Unit = 
  {
    val ods : OracleDataSource = new OracleDataSource()
    ods.setUser(identifiant)
    ods.setPassword(motDePasse)
    ods.setURL(urlBdd)
    val con : Connection = ods.getConnection()
    println("L'acteur est connecte, requete pour avoir un client")
    val resultSet : ResultSet = con.createStatement().executeQuery("SELECT * FROM les_clients")
    var compteur : Int = 1
    while (resultSet.next()) 
    {
      println(resultSet.getString(1))
      compteur = compteur + 1
    }
    con.close()
  }
  
}
