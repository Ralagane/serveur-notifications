package acteur

import akka.actor.{ Props, ActorSystem, Actor, ActorLogging, ActorRef, PoisonPill }
import model._
import model.Notification
import model.EtatDeRemiseClient
import model.EtatDeRemiseApi
import model.LeEmail.toEmail
import java.util.Date
import utilitaire.BoiteAOutils.randomStringAmeliore
import utilitaire.BoiteAOutils.genererTokenSHA1

/**
 * Un Esclave reçoit les demandes d'un AgentDesMessages, les traitent et envoie la réponse à cet agent.
 * connait : rien
 * recoit : les demandes de l'agent
 * au démarrage : ne fait rien, il attend d'être notificié.
 * Cette classe possède un objet compagnion.
 * Remarque : c'est cette classe qui est la plus complexe du système des acteurs car c'est la classe qui fait toutes les opérations de traitement
 * Moralité : ne soyez pas un esclave.
 */
object Esclave
{
  // les actions du client
  case class creerCompteDuClient(unClient : Client, client : ActorRef)
  case class notificationNonLuesClient(tokenClient : String, client: ActorRef)
  case class metteAJourEtatDeRemiseClient(unEtatDeRemise: EtatDeRemiseClient,client : ActorRef)
  case class supprimerCompteClient(tokenClient: String, client : ActorRef)
  case class sauthentifierClient(emailEtMdp: String, unClient : ActorRef)

  // les actions d'une APi
  case class creerCompteDeApi(uneApiTierce : ApiTierce, apitierce : ActorRef)
  case class envoyerUneNotificationAPI(uneNotification: Notification, apitierce : ActorRef)
  case class voirStatistiqueAPI(tokenApi : String, apitierce : ActorRef)
  case class supprimerCompteAPI(tokenApi: String, apitierce : ActorRef)
  case class consulterEtatDeRemiseApi(tokenApi : String, identifiantNotification : String, apitierce : ActorRef)

  def apply(): Props = Props(new Esclave())
}

class Esclave extends Actor with ActorLogging
{
  import BaseDeDonneesEcoPlus._
  import Esclave._
  import AgentDesMessages._

  def receive: Receive = 
  {
    case sauthentifierClient(emailEtMdp: String, unClient : ActorRef) =>
    {
      verifierClient(emailEtMdp) match
      {
        case true  => sender ! reponseAuthentification(emailEtMdp, unClient)
        case false => sender ! reponseAuthentification("erreur", unClient)
      }
    }
      /**
      * Creer un utilisateur.
      * Nous devons vérifier si le courrier électronique donné en paramètre n'existe pas pour le créer.
      * evolution : faire le traitement avec une vraie bdd.
      * 
      * @param unClient le client avec ses informations à entrer en base
      * @param client l'acteur reférent du client. En gros c'est l'utilisateur qui a fait la requête.
      */
    case creerCompteDuClient(unClient : Client, client : ActorRef) =>
    {
      verifierEmail(unClient.email) match
      {
        case true  => sender ! reponseCreerCompteClient(client,"roblox") // cas 1 : le courrier est présent, on retourne l'erreur ROBLOX !!
        case false =>                                                   // cas 2 : c'est bien un nouvel utilisateur à inscrire en base
        {
          //var tokenClient : String = "Client"++ BoiteAOutils.randomString(5) // token sha1 version eco+
          //unClient.motDePasse.replaceAll(_,"crypté") // on supprime le mdp  ?
          val tokenClient: String =  utilitaire.BoiteAOutils.genererTokenSHA1(unClient.email.++(unClient.motDePasse))
          ajouterClient(tokenClient, unClient) // tous ça en une fonction ?
          sender ! reponseCreerCompteClient(client,tokenClient)
        }
      }
    }
    /**
      * Supprimer un utilisateur.
      * Nous devons vérifier si le token donné en paramètre existe.
      * evolution : faire le traitement avec une vraie bdd.
      * 
      * @param tokenClient le token du client à supprimer.
      * @param client l'acteur reférent du client. En gros c'est l'utilisateur qui a fait la requête.
      */
    case supprimerCompteClient(tokenClient: String, client : ActorRef) =>
    {
      getClient(tokenClient) match 
      {
        case None => sender ! reponseSupprimerCompteClient(client, false)
        case Some(unClient) =>
        {
          supprimerClient(tokenClient, unClient.email) 
          sender ! reponseSupprimerCompteClient(client, true)
        }     
      } 
    }
    /**
      * Creer le compte d'une API tierce.
      * Nous devons vérifier si l'organisation existe déjà en base.
      * evolution : faire le traitement avec une vraie bdd.
      * 
      * @param uneApiTierce les informations de l'api à enregistrer en base
      * @param apitierce l'acteur reférent de l'api. En gros c'est l'utilisateur, ici l'api, qui a fait la requête.
      */
    case creerCompteDeApi(uneApiTierce : ApiTierce, apitierce : ActorRef) =>
    {
      verifierOrganisation(uneApiTierce.organisation) match 
      {
        // cas 1 : l'organisation est déjà inscrite en base. Le code erreur est PHILIPE car (il) SAIT OU IL SE CACHE
        case true  => sender ! reponseCreerCompteApi("philipe",apitierce)
        // case 2 : il faut l'enregistrer en base et renvoyer son token       
        case false =>
        {
          //var tokenApi : String = "API"++ randomString(5) le token eco+
          val tokenApi = genererTokenSHA1(uneApiTierce.email.++(uneApiTierce.motDePasse))
          ajouterAPI(tokenApi,uneApiTierce )
          sender !  reponseCreerCompteApi(tokenApi,apitierce)
        }  
      }
    }
    /**
      * Supprimer une api. 
      * Nous devons voir si elle est inscrite en base
      * evolution : faire le traitement avec une vraie bdd.
      * 
      * @param tokenApi les informations de l'api à enregistrer en base
      * @param apitierce l'acteur reférent de l'api. En gros c'est l'utilisateur, ici l'api, qui a fait la requête.
      */
    case supprimerCompteAPI(tokenApi: String, apitierce : ActorRef) =>
    { 
      getApi(tokenApi) match // comme en Haskell mais avec de l'objet
      {
        case None => sender ! reponseSupprimerCompteApi(apitierce,false)
        case Some(uneApiTierce)=>
        { 
          supprimerAPI(tokenApi, uneApiTierce.organisation)
          sender ! reponseSupprimerCompteApi(apitierce,true)
        }  
      }
    }
    /**
      * Une api veut envoyer une notification.
      * 1 - on vérifie si l'api est en base
      * 2 - on va envoyer la notification aux clients 
      * on regarde si le client est en base
      * si oui on lui envoie la notification (en fait on la stocke en base)
      * dans le cas où le client n'a pas eu de notification avant, on la crée
      * inon ça veut dire que l'email n'existe pas et donc le client nonn plus
      */
    case envoyerUneNotificationAPI(uneNotification: Notification, apitierce : ActorRef) =>
    {
      var listeClientsNonReconnues : List[String] = List.empty
      var nouvelleNotification : Notification = Notification(uneNotification.emetteur,randomStringAmeliore, uneNotification.message,uneNotification.listeDeCourriers)
      // la génération d'un id client version eco+
      verifierApi(uneNotification.emetteur) match 
      {
        case false => sender ! reponseEnvoyerUneNotificationAPI(List.empty, apitierce : ActorRef, 401) // api pas connue >:(
        case true =>
        {
          var dateDenvoi: Date = new Date()
          uneNotification.listeDeCourriers.foreach(unCourrier =>
          {
            verifierEmail(unCourrier) match
            {
              case false => 
              {
                listeClientsNonReconnues = listeClientsNonReconnues.:+(unCourrier)
              }
              case true  => 
              {
                creerNotificiationNonLue(nouvelleNotification, unCourrier, dateDenvoi)
              }
            }
          })
        }
        listeClientsNonReconnues.length.equals(uneNotification.listeDeCourriers.length) match // si aucune notification envoyée...
        {
          case false => 
          {
            val listeEmail : List[Email] = toEmail(listeClientsNonReconnues)
            sender ! reponseEnvoyerUneNotificationAPI(listeEmail, apitierce : ActorRef, 200)
          }
          case true  =>
          {
            val listeEmail : List[Email] = toEmail(listeClientsNonReconnues)
            sender ! reponseEnvoyerUneNotificationAPI(listeEmail, apitierce : ActorRef, 404)
            ajouterNotification(uneNotification.identifiantNotification, uneNotification.emetteur)
          } 
        }
      }
    }
    // le client veut reçevoir ses notifications non lues
    /**
      * On vérifie le client en base
      * on regarde s'il a des notifications
      * si oui on les envoies
      * sinon on n'envoie rien
    */
    case notificationNonLuesClient(tokenClient : String, client: ActorRef) =>
    {
      verifierClient(tokenClient) match
      {
        // cas 1 : le client n'est pas reconnu
        case false => sender ! reponseNotificationNonLues(List.empty, client, 401)
        // cas 2 : le client est reconnu
        case true  => 
        {
          var notificationAEnvoyer : List[NotificationClient] = List.empty
          getNotificationNonLues(getEmailClient(tokenClient)) match 
          {
            // cas 1 : pas de nouvelles notification
            case None => sender ! reponseNotificationNonLues(List.empty, client, 404)
            // cas 2 : le client a des notifications non lues
            case Some(uneListeDeNotificationNonLues) =>
            {
              // on doit envoyer les notifications + mettre à jour les états de remise (le cas 200)
              val dateRecu: Date = new Date()
              val etatDeRemise : String = ("Message reçue à  :"+dateRecu)
              uneListeDeNotificationNonLues.foreach(uneNotification =>
              {
                notificationAEnvoyer = notificationAEnvoyer.:+(NotificationClient(getNomOrganisation(uneNotification.emetteur),uneNotification.identifiantNotification,uneNotification.message,etatDeRemise))
                mettreAJourEtatDeRemise(uneNotification.identifiantNotification, tokenClient, etatDeRemise)
                ajouterNotificationLue(tokenClient, uneNotification)
              })
            sender ! reponseNotificationNonLues(notificationAEnvoyer, client, 200) 
            }
          }
          
        }
      }
    }
    // le client veut mettre à jour un état de remise sur une notification
    case metteAJourEtatDeRemiseClient(unEtatDeRemiseClient: EtatDeRemiseClient, client : ActorRef) =>
    {
      /**
       * Vérifier si le client existe.
       * Chercher la notification pour changer son état de remise.
       * Si elle existe, la changer
      */
      verifierClient(unEtatDeRemiseClient.tokenClient) match
      {
        case false => sender ! reponsemetteAJourEtatDeRemise(client, 401)
        case true  =>
        {
         getNotificationLue(unEtatDeRemiseClient.identifiantNotification) match
          {
            case None => sender ! reponsemetteAJourEtatDeRemise(client, 402)
            case Some(uneNotification)  => 
            {
              mettreAJourEtatDeRemise(unEtatDeRemiseClient.identifiantNotification, unEtatDeRemiseClient.tokenClient , unEtatDeRemiseClient.nouveauEtatDeRemise) match
              {
                case false =>  sender !  reponsemetteAJourEtatDeRemise(client, 403)
                case true  =>  sender ! reponsemetteAJourEtatDeRemise(client, 200)
              }
            }
          }
        }
      }  
    }
    /**
      * L'api veut consulter les états de remise d'une Notification.
      * 
      */
    case consulterEtatDeRemiseApi(tokenApi : String, identifiantNotification : String, apitierce : ActorRef) =>
    {
      verifierApi(tokenApi) match
      {
        case false => sender ! reponseConsulterEtatDeRemise(List.empty, apitierce, 401)
        case true  =>
        {
          verifierNotification(identifiantNotification) match
          {    
            case false =>sender ! reponseConsulterEtatDeRemise(List.empty, apitierce, 404)
            case true  => 
            verifierNotificationDeLapi(tokenApi, identifiantNotification) match
            {
              case false => sender ! reponseConsulterEtatDeRemise(List.empty, apitierce, 403)
              case true  => sender ! reponseConsulterEtatDeRemise(consulterLesEtatsDeRemiseApi(identifiantNotification, tokenApi), apitierce, 200)
            }
          }
        }
      }
    }
    /**
      * Une api veut avoir des statistiques sur ses notifications
      */
    case voirStatistiqueAPI(tokenApi : String, apitierce : ActorRef) =>
    {
      verifierApi(tokenApi) match 
      {
        // cas 1 : l'api n'est pas reconnue et il s'agit d'un espion russe.
        case false => sender ! reponseVoirStatistique(List.empty, apitierce, 401)
        case true  => 
        {
          getStatistique(tokenApi) match
          {
            case Nil => sender ! reponseVoirStatistique(List.empty, apitierce, 404)
            case uneListeDeStatistique =>sender ! reponseVoirStatistique(uneListeDeStatistique, apitierce, 200)
          }
        }
      }
    } 
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }
}
