package acteur

import model._
import model.Notification // obligé de préciser la notification car le compilateur confond avec un type Notification en javax
import java.util.Date
/**
  * Cet objet est la la base de données eco+ de notre API. Elle permet de faire abstraction d'une vraie base de données
  * pour rendre plus simple la recherche de données.
  */
object BaseDeDonneesEcoPlus
{
  var listeEmail: List[String] = List.empty // La liste des utilisateurs inscrient en base
  var listeOrganisation: List[String] = List.empty // la liste des organisations inscrite en base
  var listeClient: Map[String,Client] =  Map.empty // un client mappé avec son token
  var listeApitiere: Map[String,ApiTierce] =  Map.empty // une api mappé avec son token
  var mapEmailIDclient: Map[String,String] = Map.empty
  var listeNotificationNonLues: Map[String, List[Notification]] = Map.empty // un client a une liste de notifications non lues
  var listeNotificationsLues: Map[String, List[Notification]] = Map.empty // un client a une liste de notificiations lues.
  var mapIDNotificationStatistique : Map [String,Statistique] = Map.empty
  var mapIDApiListeIDNotification : Map [String, List[String]] = Map.empty
  var listeNotifications: List[String] = List.empty // la liste de toutes les notifications existantes en base
  var listeEtatDeRemise: Map[String, List[EtatDeRemise]] =  Map.empty // l'identifiant d'une notification + une liste d'états de remise par client
  
/////////// fonctions sur le client
  def ajouterClient(unTokenClient : String, unClient : Client): Unit =
  {
    listeClient = listeClient.+(unTokenClient -> unClient)
    listeEmail = listeEmail.:+(unClient.email)
    mapEmailIDclient = mapEmailIDclient.+(unClient.email->unTokenClient)
  }

  def supprimerClient(tokenClient : String, emailClient : String) : Unit =
  {
    // grand nettoyage de printemps : on supprime TOUTES traces du client en base.
    listeClient = listeClient.-(tokenClient)
    listeEmail = listeEmail.filter(_.equals(emailClient)) 
    listeNotificationNonLues = listeNotificationNonLues.-(tokenClient)
    listeNotificationsLues = listeNotificationsLues.-(tokenClient)
    //supprimmer tous les états de remise du client par notification
    listeEtatDeRemise.foreach(unEtatDeRemise =>unEtatDeRemise._2.filter(_.identifiantClient.equals(tokenClient)))
  }
  def verifierClient(unTokenClient : String) : Boolean =
  {
    return listeClient.contains(unTokenClient)
  }

  def getClient(unTokenClient :String) : Option[Client] =
  {
    listeClient.get(unTokenClient)
  }

  def getEmailClient(unTokenClient : String) : String =
  {
    listeClient.get(unTokenClient).get.email 
  }
  
  def verifierEmail(unEmail : String) : Boolean = 
  {
    return listeEmail.contains(unEmail)
  }

/////////// fonctions sur l'API
  def verifierOrganisation (uneOrganisation : String) : Boolean =
  {
    listeOrganisation.contains(uneOrganisation)
  }

  def ajouterAPI (unTokenApi : String, uneApiTierce : ApiTierce) : Unit =
  {
    listeApitiere = listeApitiere.+(unTokenApi -> uneApiTierce)
    listeOrganisation = listeOrganisation.:+(uneApiTierce.organisation)
  }

  def supprimerAPI(unTokenApi : String, uneOrganisation : String) : Unit =
  {
    listeOrganisation = listeOrganisation.filter(_.equals(uneOrganisation))
    listeApitiere = listeApitiere.-(unTokenApi)
  }

  def verifierApi(unTokenApi : String) : Boolean =
  {
    listeApitiere.contains(unTokenApi)
  }

  def getApi(tokenApi : String) : Option[ApiTierce] =
  {
    listeApitiere.get(tokenApi)
  }

  def getEmailApi(tokenApi : String) : String =
  {
    listeApitiere.get(tokenApi).get.email
  }

  def getNomOrganisation(tokenApi : String) : String = 
  {
    listeApitiere.get(tokenApi).get.organisation
  }

/////////// fonctions sur les notifications
  def ajouterNotification (identifiantNotification: String, tokenApi : String)
  {
     listeNotifications = listeNotifications.:+(identifiantNotification)
     mapIDApiListeIDNotification.get(tokenApi) match 
     {
       case None => mapIDApiListeIDNotification = mapIDApiListeIDNotification.+(tokenApi -> List(identifiantNotification))
       case Some(uneListeDeIDNotification) =>mapIDApiListeIDNotification = mapIDApiListeIDNotification.+(tokenApi -> uneListeDeIDNotification.:+(identifiantNotification))
     } 
  }

  def ajouterNotificationLue(unTokenClient : String, uneNotification : Notification)
  {
    listeNotificationsLues = listeNotificationsLues.+(unTokenClient ->List(uneNotification))
    listeNotificationNonLues = listeNotificationNonLues.-(getEmailClient(unTokenClient))
  }

  def verifierNotification(identifiantNotification : String) : Boolean =
  {
    return listeNotifications.contains(identifiantNotification)
  }

  def getNotificationLue(identifiantNotification : String) : Option[List[Notification]] =
  {
    listeNotificationsLues.get(identifiantNotification)
  }

  def getNotificationNonLues(unEmail : String) : Option[List[Notification]] =
  {
    listeNotificationNonLues.get(unEmail)
  }

  def creerNotificiationNonLue(uneNotification : Notification, unEmail : String, uneDate : Date ) : Unit =
  {
    getNotificationNonLues(unEmail) match // on regarde si le client possède déjà des notifications à lire
    {
      case None => 
      {
        listeNotificationNonLues = listeNotificationNonLues.+(unEmail -> List(uneNotification))
        listeEtatDeRemise = listeEtatDeRemise.+(uneNotification.identifiantNotification -> List(EtatDeRemise(mapEmailIDclient.get(unEmail).get,("Message envoyé à  :"+uneDate))))
        // générer stat ici

      }
      case Some(uneListeDeNotification) =>
      {
        val lanouvelleliste = listeNotificationNonLues.get(unEmail).get ++ List(uneNotification)
        listeNotificationNonLues = listeNotificationNonLues.-(unEmail)
        listeNotificationNonLues = listeNotificationNonLues.+(unEmail ->lanouvelleliste )
        listeEtatDeRemise = listeEtatDeRemise.+(uneNotification.identifiantNotification -> List(EtatDeRemise(mapEmailIDclient.get(unEmail).get,("Message envoyé à  :"+uneDate))))
        // générer stat ici
      }
    }
  }

  def mettreAJourEtatDeRemise(identifiantNotification : String, unTokenClient : String, nouveauEtatDeRemise : String) : Boolean =
  {
    var ok : Boolean = false
    BaseDeDonneesEcoPlus.listeEtatDeRemise.get(identifiantNotification).get.foreach(unEtatDeRemise => 
    {
      unEtatDeRemise.identifiantClient.equals(unTokenClient) match
      {
        case true => 
          unEtatDeRemise.etatDeRemise.replaceAll(_, nouveauEtatDeRemise)
          ok = true
        case false =>{}
      }
    })
    ok
  }

  def recevoirEmailParLeTokenClient(tokenClient : String) : String =
  {
    return listeClient.get(tokenClient).get.email
  }

  def verifierNotificationDeLapi(tokenApi : String, identifiantNotification : String) : Boolean =
  {
    var ok :Boolean = false
    mapIDApiListeIDNotification.get(tokenApi) match 
    {
      case None => false
      case Some(uneListeDeIDNotification) => 
      uneListeDeIDNotification.foreach(unIDNotification =>
      {
        unIDNotification.equals(identifiantNotification) match 
        {
          case true  => ok = true
          case false =>
        }
      })
    }
    ok
  }
  def consulterLesEtatsDeRemiseApi(identifiantNotification : String, tokenApi : String) : List[EtatDeRemiseApi] = 
  {
    var listeEtatDeRemiseApi : List[EtatDeRemiseApi]= List.empty
    listeEtatDeRemise.get(identifiantNotification).get.foreach(unEtatDeRemise =>
    {
      listeEtatDeRemiseApi = listeEtatDeRemiseApi.:+(EtatDeRemiseApi((recevoirEmailParLeTokenClient(unEtatDeRemise.identifiantClient)),unEtatDeRemise.etatDeRemise))
    })
    listeEtatDeRemiseApi
  }
    
  def getStatistique(tokenApiTierce :String) : List[Statistique] =
  { 
    var listeStatistique : List[Statistique] = List.empty
    mapIDApiListeIDNotification.get(tokenApiTierce) match 
    {
      case None => listeStatistique
      case Some(uneListeDeIDNotification) =>
      {
        uneListeDeIDNotification.foreach(unIDNotification =>
        {
          listeStatistique = listeStatistique.:+(mapIDNotificationStatistique.get(unIDNotification).get)
        })
        listeStatistique
      }
    }
  }
  // on ne va pas utiliser un type "tokenclient" (edit : en fait si Todo : l'implémenter)
  def consulterInformationClient(tokenClient : String) : Map[Boolean,Client] = 
  {
    // si  le client existe en base on le renvoie sinon c'est un échec.
    listeClient.get(tokenClient) match
    {
      case Some(unClient) => Map(true -> unClient)
      case None           => Map(false -> Client("","",1337,"","",""))
    }
  }

  def consulterInformationAPI(tokenApi : String): Map[Boolean,ApiTierce] =
  {
    listeApitiere.get(tokenApi) match
    {
      case Some(uneApi) => Map(true -> uneApi)
      case None         => Map(false -> ApiTierce("","",""))
    }
  } 
}
