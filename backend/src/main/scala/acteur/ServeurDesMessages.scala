package acteur

import akka.actor.{ Props, ActorSystem, Actor, ActorLogging, ActorRef, PoisonPill }


// la classe maitre qui initialise le tout
class ServeurDesMessages(system: ActorSystem)
{
  val esclave = system.actorOf(Esclave(),name ="lePauvreEsclave") 
  val agentDesMessages= system.actorOf(AgentDesMessages(esclave),name="unAgent") 
  val unClient = system.actorOf(LeClient(agentDesMessages),name = "unClient") // acteur test (pour tester mes requêtes)
}
