Il s'agit du projet universitaire. Le but est de concevoir un serveur de notification en scala, angular et swagger avec un système de back-end et front-end. Le projet mérite un ensemble d'améliorations que je détaillerai à la fin de ce readme. À l’heure actuelle, le Back-End est plus abouti que le frontend.

## Back-End
La Back-End est programmé en Scala utilisant akka-http et les acteurs. L'api est générée par swagger editor.

Concernant le système d'acteur, celui-ci fonctionne avec plusieurs acteurs :
- nous avons l'acteur "serveur" qui instancialise les acteurs
- l'acteur "esclave" chargé de traiter toutes les demandes (il était prévu qu'il y ait un ensemble d'esclaves pour traiter plusieurs clients)
- un "agent des messages" chargé de déléguer le travail à l'esclave et devant envoyer les réponses de l'esclave à un client (lui aussi devait avoir un ensemble d'agents pour plusieurs clients)
- une base de données sous oracle qui traite les requêtes de l'esclave. Il était prévu qu'une liste de script sql permet de créer une base de données automatiquement avec un utilisateur "acteur_bdd" pour permettre à notre acteur d'utiliser la base, des tables et un jeu de données. Ensuite mettre en place une base de données sous docker. Pour le moment cet acteur n'est pas utilisé et seul une classe "base de données eco+" est utilisée dans la mesure où elle stocke dans des variables les données avec des fonctions les manipulant pour faciliter le travail de l'esclave. Il est prévu de remplacer cette base de données par un moteur de recherche et une ETL (logstash et elasticsearch)

## Front-End
Le Front-End est implémenté en Angular 10. Il est relié au Back-End via une partie des routes fonctionnelles. Pour le moment une page est implémentée permettant à un utilisateur de s'inscrire ou de
s'identifier et après une authentification, et si le client possède une liste de notifications non lues, le serveur lui affichera cette liste.

# Architecture du dépôt
Vous trouverez à la racine un fichier .yaml résultant de la spécification sur swagger.
Vous trouverez dans le fichier "backend" l'api générée par swagger et le système des acteurs. Vous trouverez dans le fichier "frontend" la partie Front-End en Angular 10.

# Critique du projet et améliorations
Voilà une liste non exhaustive de tout ce qu'il faut faire pour améliorer le projet :
- Le Front-End n’est pas tout à fait au point (il faut un meilleur visuel et une meilleure gestion des sessions utilisateurs). J’ai décidé de l’améliorer plus tard, quand j’aurai acquis une meilleure expérience en Angular,
- La base de données de substitution a été faite pour gagner du temps et j’avais décidé d’améliorer cet aspect pour avoir une vraie base de données sous oracle,
- Lors de la création d’un compte, le mot de passe n’est pas crypté,
- Une meilleure gestion des erreurs côté Front-End,
- Il faut pouvoir créer plus d’acteurs quand il y a beaucoup d’utilisateurs sur le site,
- Il faut une page spéciale pour les api tierce afin qu’elle puisse envoyer leurs messages et recevoir les états de remise ou statistique. À l’heure actuelle, on doit passer par CURL pour faire les requêtes,
- Il faut implémenter la possibilité à une Application tierce de consulter les statistiques de ses messages envoyés,
- Mettre en place des pipelines de tests au sein du git.


