import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; // pour communiquer avec le serv ("remplacer le curl")
import { UnClient } from '../models/unclient';
import { UneApiTierce} from '../models/uneapitierce';
import { UneNotification } from '../models/uneNotification';
import { NotificationClient } from '../models/notificationClient';
// c'est la qu'on fait le lien avec le serveur
@Injectable({
  providedIn: 'root'
})
export class CommunicationService
{

  serveur = 'http://localhost:4200/api/';

  constructor(private httpClient: HttpClient) { } // pour initialiser les imports et les utiliser dans le code.

  public async postAuthentification(email: string, motDePasse: string) : Promise<Object>
  {
    return await this.httpClient.get(this.serveur + 'authentification/'+email+'/'+motDePasse).toPromise().catch((e) => {
      return e;
    });;
  }

  public async postCreerCompteClient(client: UnClient): Promise<Object> 
  {
    let body = '{"nom": "' + client.nom + '", "prenom": "' + client.prenom + '", "age": ' + client.age + ', "sexe": "' + client.sexe + '", "email":"'+client.email+'", "motDePasse":"' +client.motDePasse+ '"}';
    const options = {headers: {'Content-Type': 'application/json'}};
    return await this.httpClient.post(this.serveur + 'creercompteclient', body, options).toPromise().catch((e) => {
      return e;
    });
  }

  public async postCreerCompteApi(uneApi : UneApiTierce)
  {
    const options = {headers: {'Content-Type': 'application/json'}};
    let body = '{organisation:'+uneApi.organisation+', motdepasse:'+uneApi.motDePasse+', email:'+uneApi.email+'}';
    return await this.httpClient.post(this.serveur + 'creercompteapi/', JSON.stringify(body), options).toPromise();
  }

  public async postEnvoyerNotification(uneNotification : UneNotification)
  {
    const options = {headers: {'Content-Type': 'application/json'}};
    let body = '{emetteur:'+uneNotification.emetteur+', identifiantNotification:'+uneNotification.identifiantNotification+', message:'+uneNotification.message+', listeDeCourriers:'+uneNotification.listeDeCourriers+'}';
    return await this.httpClient.post(this.serveur + 'envoyernotification', body, options).toPromise();
  }

  public async getNotificationsNonLues(uneCleClient : string): Promise<NotificationClient> // virer le any trop dangereux à virer
  {
    return await this.httpClient.get(this.serveur + 'notificationsnonLues/' + uneCleClient).toPromise().catch((e) => {
      return e;
    });
  }

  public async getEtatDeRemise(uneCleApi : string, identifiantNotification : String) : Promise<Object>
  {
    return await this.httpClient.get(this.serveur + 'etatderemise/' + uneCleApi+'/'+identifiantNotification).toPromise();
  }

  public async getStatistique(unecleApi : string)
  {
    return this.httpClient.get(this.serveur + 'statistique/' + unecleApi).toPromise();
  }

  //async /supprimercompte/{TokenApiTierce} /supprimercompte/{TokenClient}
  public async deleteSupprimerCompteClient(uneCleClient : String)
  {
    return this.httpClient.get(this.serveur + 'supprimercompteclient/' + uneCleClient).toPromise();
  }

  public async deleteSupprimerCompteApi(unecleApi : string)
  {
    return this.httpClient.get(this.serveur + 'supprimercompteapi/' + unecleApi).toPromise();
  }
///////////////////////////////

  public async authentifier(email : string, motDePasse : string)
  {
    const res: any  = await this.postAuthentification(email, motDePasse)
    if(res.status == 200)
    {
      return res.error.text;
    }
    return "erreur";
  }
  public async creerCompteClient(client: UnClient): Promise<Object>  // virer trop dangereux à virer
  {
    const res: any =  await this.postCreerCompteClient(client);
    return res.error.text;
  }

  public async creerCompteApi(uneApi : UneApiTierce)
  {
    const res = await this.postCreerCompteApi(uneApi);
    if(res) return res;
    else return 'ERROR';
  }

  public async envoyerNotificaiton(uneNotification : UneNotification)
  {
    const res = await this.postEnvoyerNotification(uneNotification);
    if(res) return true;
    else return 'ERROR :'+res;
  }

  public async notificationsNonLues(uneCleClient : string) : Promise<NotificationClient>
  {
    const res: any  = await this.getNotificationsNonLues(uneCleClient);
    if(res.length >= 1)
    {
      return res
    }
    return new NotificationClient();
  }

  public async etatDeRemise(uneCleApi : string, identifiantNotification : String)
  {
    const res = await this.getEtatDeRemise(uneCleApi, identifiantNotification);
    if(res) return res;
    else return 'ERROR';
  }

  public async statistique(unecleApi : string)
  {
    const res = await this.getStatistique(unecleApi);
    if(res) return res;
    else return 'EL ERRORR'+res;
  }

  public async supprimerCompteClient(uneCleClient : String)
  {
    const res  = await this.deleteSupprimerCompteClient(uneCleClient);
    if(res) return res;
    else return 'EL ERRORR'+res;
  }

  public async supprimerCompteApi(unecleApi : string)
  {
    const res = await this.deleteSupprimerCompteApi(unecleApi);
    if(res) return res;
    else return 'EL ERROR'+res;
  }
}
