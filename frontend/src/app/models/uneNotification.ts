export class UneNotification
{
  emetteur : string = "";
  identifiantNotification : string = "";
  message : string = "";
  listeDeCourriers : Array<String> = [];

  constructor(emetteur : string, identifiantNotification : string, message : string, listeDeCourriers : Array<String>)
  {
    this.emetteur = emetteur;
    this.identifiantNotification = identifiantNotification;
    this.message = message;
    this.listeDeCourriers = listeDeCourriers;
  }
}
