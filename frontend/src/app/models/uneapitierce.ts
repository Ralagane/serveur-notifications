export class UneApiTierce
{
  organisation: String = "";
  motDePasse: String = "";
  email: String = "";

  constructor(organisation: string, motDePasse: string, email: string)
  {
    this.organisation = organisation ;
    this.motDePasse = motDePasse;
    this.email = email;
  }

}
