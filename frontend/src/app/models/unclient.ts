// la classe client
export class UnClient
{
  nom : string = "toto";
  prenom : string = "tata";
  age : number = -6;
  sexe: string = "non binaire";
  email: string = "toto@tata.com";
  motDePasse: string = "toto";

  constructor(nom: string, prenom: string, age : number, sexe : string, email : string, motDePasse: string)
  {
    this.nom = nom ;
    this.prenom = prenom;
    this.age = age
    this.sexe = sexe;
    this.email = email;
    this.motDePasse= this.motDePasse;
  }

}


