import { Component, OnInit } from '@angular/core';
import { UnClient } from '../models/unclient';
import { UneNotification } from '../models/uneNotification';
import { CommunicationService } from '../services/communication.service';
@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent
{
  isConnected : boolean = false;
  authentificationfail: boolean = false;
  token: string = '';
  listeNotificationsNonLues : any;
  nombredeNotificationsNonLues : number = 0;
  constructor(private laCom : CommunicationService){}

  async onCreation(nom : string, prenom : string, age : string, sexe : string, email : string, mdp:string) : Promise<void>
  {
    // il faudrait crypter le mdp ici.
    let client = new UnClient(nom,prenom,Number(age),sexe,email,mdp);
    let res = await this.laCom.creerCompteClient(client);
    if(res)
    {
      this.isConnected = true;
      this.token = String(res);
    }
  }

  async onAuthentification(email : string, motDePasse : string)
  {
    let res = await this.laCom.authentifier(email, motDePasse)
    if (res == "erreur")
    {
      this.authentificationfail = true;
    }
    else
    {
      this.token = res;
      this.listeNotificationsNonLues = await this.laCom.notificationsNonLues(this.token)
      this.nombredeNotificationsNonLues = this.listeNotificationsNonLues.length;
      this.isConnected = true;
    }
  }

}
