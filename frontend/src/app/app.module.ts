import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { HttpClientModule } from '@angular/common/http'; // pour communiquer avec le serv ("remplacer le curl")
import { PageDesNotificationsComponent } from './page-des-notifications/page-des-notifications.component';

@NgModule({
  declarations:
  [	
    AppComponent,
    AccueilComponent,
      PageDesNotificationsComponent
   ],
  imports:
  [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
